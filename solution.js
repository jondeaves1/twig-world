/**
 * Takes in an array and a numeric value, splits array into sub-arrays
 *
 * @param {Array} arr Array of items to be split
 * @param {Integer} numOfArrays Number of items array should be divided into
 */
const arrayDivider = function arrayDivider(arr, numOfArrays) {
    let outArr = [];
    let iItems = 0;

    // Round up to add our remainder into the final part
    let itemsPerPos = Math.ceil(arr.length / numOfArrays, 10);

    for (iItems; iItems < numOfArrays; iItems++) {
        // Take sub-array from main array and remove from original
        // Ensures zero index will always be the latest
        const addItem = arr.splice(0, itemsPerPos);
        if (addItem.length > 0) {
            outArr.push(addItem);
        }
    }

    return outArr;
}

// Export the implemented function default
module.exports = arrayDivider;