# Twig World
## Skills test

### Overview

Given an array of length >= 0, and a positive integer N, return the contents of the array divided into N equally sized arrays.

Where the size of the original array cannot be divided equally by N, the final part should have length equal to the remainder.

**Example:** <br>
Input
```js
arrayDivider([1, 2, 3, 4, 5], 3)
``` 
Output
```js
[[1, 2], [3, 4], [5]]
```

### Solution
My solution makes use of TDD using Mocha and Chai to build a function that returns the expected array based on a variety of inputs.

### Requirements
The example here will require a recent version of node that supports ES6 syntax, LTS will suffice.

### Installation
Run the following command to install the local dependencies.
```shell
npm install
```

### Running
The example here makes use of NPM scripts to run the unit tests and the code itself. To run the solution with single example execute:
```shell
npm start
```

To run the full list of unit tests execute:
```shell
npm test
```