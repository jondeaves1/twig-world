const chai = require('chai');
const should = chai.should();
const arrayDivider = require('./solution');

describe('Array divider', () => {
  it('should return an array with one part when array count is 1', () => {
    const result = arrayDivider([1], 1);

    // Ensures that no more items are in array than required
    result.should.have.a.lengthOf(1);

    // Ensure the expected item in array matches exactly to what is actually returned
    result[0].should.deep.equal([1]);
  });
  
  it('should return an array with one non numeric part when array count is 1', () => {
    const result = arrayDivider(['foo'], 1);

    result.should.have.a.lengthOf(1);
    result[0].should.deep.equal(['foo']);
  });

  it('should return an array with 2 parts', () => {
    const result = arrayDivider([1, 2], 2);

    result.should.have.a.lengthOf(2);
    result[0].should.deep.equal([1]);
    result[1].should.deep.equal([2]);
  });

  it('should return an array with 3 parts', () => {
    const result = arrayDivider([1, 2, 3, 4, 5, 6], 3);

    result.should.have.a.lengthOf(3);
    result[0].should.deep.equal([1, 2]);
    result[1].should.deep.equal([3, 4]);
    result[2].should.deep.equal([5, 6]);
  });

  it('should return an array with 3 parts, the last item containing only one parts', () => {
    const result = arrayDivider([1, 2, 3, 4, 5], 3);

    result.should.have.a.lengthOf(3);
    result[0].should.deep.equal([1, 2]);
    result[1].should.deep.equal([3, 4]);
    result[2].should.deep.equal([5]);
  });

  it('should return an array with 3 parts, the last item containing only two parts', () => {
    const result = arrayDivider([1, 2, 3, 4, 5, 6, 7, 8, 9, 10], 3);

    result.should.have.a.lengthOf(3);
    result[0].should.deep.equal([1, 2, 3, 4]);
    result[1].should.deep.equal([5, 6, 7, 8]);
    result[2].should.deep.equal([9, 10]);
  });

  it('should return an array with 4 parts', () => {
    const result = arrayDivider([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12], 4);

    result.should.have.a.lengthOf(4);
    result[0].should.deep.equal([1, 2, 3]);
    result[1].should.deep.equal([4, 5, 6]);
    result[2].should.deep.equal([7, 8, 9]);
    result[3].should.deep.equal([10, 11, 12]);
  });

  it('should return an array with 4 parts, the last item containing only one item', () => {
    const result = arrayDivider([1, 2, 3, 4, 5, 6, 7, 8, 9, 10], 4);

    result.should.have.a.lengthOf(4);
    result[0].should.deep.equal([1, 2, 3]);
    result[1].should.deep.equal([4, 5, 6]);
    result[2].should.deep.equal([7, 8, 9]);
    result[3].should.deep.equal([10]);
  });

  it('should return an array with 5 parts', () => {
    const result = arrayDivider([1, 2, 3, 4, 5, 6, 7, 8, 9, 10], 5);

    result.should.have.a.lengthOf(5);
    result[0].should.deep.equal([1, 2]);
    result[1].should.deep.equal([3, 4]);
    result[2].should.deep.equal([5, 6]);
    result[3].should.deep.equal([7, 8]);
    result[4].should.deep.equal([9, 10]);
  });

  it('should return an array with 5 parts, the last item containing only one item', () => {
    const result = arrayDivider([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13], 5);

    result.should.have.a.lengthOf(5);
    result[0].should.deep.equal([1, 2, 3]);
    result[1].should.deep.equal([4, 5, 6]);
    result[2].should.deep.equal([7, 8, 9]);
    result[3].should.deep.equal([10, 11, 12]);
    result[4].should.deep.equal([13]);
  });
});