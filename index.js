const arrayDivider = require('./solution.js');

const results = arrayDivider([1, 2, 3, 4, 5], 3);

console.log('Inputs: ([1, 2, 3, 4, 5], 3)');
console.log(`Outputs:`, results);